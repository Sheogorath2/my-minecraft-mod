package org.gera.mymod.generation;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenMinable;
import cpw.mods.fml.common.IWorldGenerator;

public class MyGenerator implements IWorldGenerator {
	
	private int rarity;
	private Block ore;
	private int size;
	private int height;

	public MyGenerator(Block ore, int rarity, int size, int height)	{
		this.rarity = rarity;
		this.ore = ore;
		this.size = size;
		this.height = height;
	}
	
	@Override
	public void generate(Random random, int chunkX, int chunkZ, World world,
			IChunkProvider chunkGenerator, IChunkProvider chunkProvider) {
		
		switch(world.provider.dimensionId)	{
		case -1:
			generateInNether(world, random, chunkX*16, chunkZ*16);
		case 0:
			generateInOverworld(world, random, chunkX*16, chunkZ*16);
		case 1:
			generateInEnd(world, random, chunkX*16, chunkZ*16);
			
		}
		
	}

	private void generateInEnd(World world, Random random, int x, int z) {
		
	}

	private void generateInOverworld(World world, Random random, int x, int z) {
		for (int i = 0; i<rarity; i++)	{
			int chunkX = x + random.nextInt(16);
			int chunkY = random.nextInt(height);
			int chunkZ = z + random.nextInt(16);
			
		(new WorldGenMinable(this.ore, this.size)).generate(world, random, chunkX, chunkY, chunkZ);
		}
	}

	private void generateInNether(World world, Random random, int x, int z) {
		
	}

}
