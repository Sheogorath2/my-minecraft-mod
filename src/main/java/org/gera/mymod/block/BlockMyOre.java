package org.gera.mymod.block;

import java.util.Random;

import org.gera.mymod.Init;
import org.gera.mymod.MyRegistry;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class BlockMyOre extends Block {

	public BlockMyOre(Material p_i45394_1_) {
		super(p_i45394_1_);
		setHardness(5F);
		setResistance(7F);
		setBlockTextureName(Init.MODID + ":BlockMyOre");
	}
	
	@Override
	public Item getItemDropped(int par1, Random par2Random, int par3)	{
		return MyRegistry.MyItem;
	}

}
