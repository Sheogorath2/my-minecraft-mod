package org.gera.mymod.block;

import org.gera.mymod.Init;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockMyFurnace extends BlockContainer {

	private final boolean isActive;
	
	@SideOnly(Side.CLIENT)
	private IIcon iconFront;
	@SideOnly(Side.CLIENT)
	private IIcon iconTop;
	
	public BlockMyFurnace(boolean isActive, Material p_i45386_1_) {
		super(p_i45386_1_);
		setHardness(2F);
		setResistance(4F);
		this.isActive = isActive;
	}

	@Override
	public TileEntity createNewTileEntity(World var1, int var2) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister iconRegistry)	{
		this.blockIcon = iconRegistry.registerIcon(Init.MODID + ':' + "BlockMyFurnace_side");
		this.iconFront = iconRegistry.registerIcon(Init.MODID + ':' + (this.isActive ? "BlockMyFurnace_front_on" : "BlockMyFurnace_front_off"));
		this.iconTop = iconRegistry.registerIcon(Init.MODID + ':' + "BlockMyFurnace_top");
	}

}
