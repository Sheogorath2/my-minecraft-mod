package org.gera.mymod.block;

import org.gera.mymod.Init;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class BlockMyBlock extends Block {

	public BlockMyBlock(Material p_i45394_1_) {
		super(p_i45394_1_);
		//setCreativeTab(CreativeTabs.tabBlock);
		setHardness(5F);
		setResistance(7F);
		setBlockTextureName(Init.MODID + ":BlockMyBlock");
	}

}
