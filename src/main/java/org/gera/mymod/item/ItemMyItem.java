package org.gera.mymod.item;

import org.gera.mymod.Init;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemMyItem extends Item {
	
	public ItemMyItem()		{
		setCreativeTab(CreativeTabs.tabMisc);
		setTextureName(Init.MODID + ":ItemMyItem");
	}
	
}
