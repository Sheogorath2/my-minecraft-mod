package org.gera.mymod.item;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;

import org.gera.mymod.Init;
import org.gera.mymod.MyRegistry;

public class ItemMyFood extends ItemFood {

	public ItemMyFood(int hunger, float saturation, boolean isWolfFood) {
		super(hunger, saturation, isWolfFood);
		setCreativeTab(MyRegistry.MyTab);
		setTextureName(Init.MODID + ":ItemMyFood");
		setPotionEffect(Potion.poison.id, 1, 1, 0.5F);
	}
	
	protected void onFoodEaten(ItemStack itemstack, World world, EntityPlayer player)	{
		player.addPotionEffect(new PotionEffect(Potion.jump.id, 100, 3));
		player.addPotionEffect(new PotionEffect(Potion.moveSpeed.id, 100, 2));
		player.addPotionEffect(new PotionEffect(Potion.resistance.id, 100, 1));
	}

}
