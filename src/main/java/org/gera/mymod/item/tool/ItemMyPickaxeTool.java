package org.gera.mymod.item.tool;

import org.gera.mymod.Init;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemPickaxe;

public class ItemMyPickaxeTool extends ItemPickaxe {

	public ItemMyPickaxeTool(ToolMaterial p_i45327_1_) {
		super(p_i45327_1_);
		//setCreativeTab(CreativeTabs.tabTools);
		setTextureName(Init.MODID + ":ItemMyPickaxeTool");
	}

}
