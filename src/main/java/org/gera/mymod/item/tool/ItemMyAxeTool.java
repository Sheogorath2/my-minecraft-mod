package org.gera.mymod.item.tool;

import org.gera.mymod.Init;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemAxe;

public class ItemMyAxeTool extends ItemAxe {

	public ItemMyAxeTool(ToolMaterial p_i45327_1_) {
		super(p_i45327_1_);
		//setCreativeTab(CreativeTabs.tabTools);
		setTextureName(Init.MODID + ":ItemMyAxeTool");
	}

}
