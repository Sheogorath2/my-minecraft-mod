package org.gera.mymod.item.tool;

import org.gera.mymod.Init;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemSword;

public class ItemMySwordTool extends ItemSword {

	public ItemMySwordTool(ToolMaterial p_i45327_1_) {
		super(p_i45327_1_);
		//setCreativeTab(CreativeTabs.tabCombat);
		setTextureName(Init.MODID + ":ItemMySwordTool");
	}

}
