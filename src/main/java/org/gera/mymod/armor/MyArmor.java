package org.gera.mymod.armor;

import net.minecraft.entity.Entity;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;

import org.gera.mymod.Init;
import org.gera.mymod.MyRegistry;

public class MyArmor extends ItemArmor {

	public MyArmor(ArmorMaterial material, int id, int placement) {
		super(material, id, placement);
		
		setCreativeTab(MyRegistry.MyTab);
		
		if (placement == 0)		{
			setTextureName(Init.MODID + ":ItemMyArmorHelmet");
		}
		else if (placement == 1)		{
			setTextureName(Init.MODID + ":ItemMyArmorChestplate");
		}
		else if (placement == 2)		{
			setTextureName(Init.MODID + ":ItemMyArmorLeggins");
		}
		else if (placement == 3)		{
			setTextureName(Init.MODID + ":ItemMyArmorBoots");
		}
	}
	
	public String getArmorTexture(ItemStack stack, Entity entity, int slot, String type)	{
		if (stack.getItem() == MyRegistry.myArmorHelmet ||
				stack.getItem() == MyRegistry.myArmorChestplate ||
				stack.getItem() == MyRegistry.myArmorBoots)		{
			return Init.MODID + ":textures/models/armor/MyArmor_1.png";
		}
		else if (stack.getItem() == MyRegistry.myArmorLeggins) 	{
			return Init.MODID + ":textures/models/armor/MyArmor_2.png";
		}
		return " ";
	}
}
