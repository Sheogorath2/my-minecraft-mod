package org.gera.mymod;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.registry.GameRegistry;

@Mod(modid = Init.MODID, name = Init.NAME, version = Init.VERSION)
public class Init {
	//Mod Info
	public static final String MODID = "mymod";
	public static final String NAME = "This is my mod";
	public static final String VERSION = "v1.0";
	
	public Init()	{
		MyRegistry.registerGenerators();
		MyRegistry.registerStuff();
		MyRegistry.registerCraft();
	}
}
