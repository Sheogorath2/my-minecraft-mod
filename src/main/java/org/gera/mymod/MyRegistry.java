package org.gera.mymod;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraftforge.common.util.EnumHelper;

import org.gera.mymod.CreativeTabs.TabMyTab;
import org.gera.mymod.armor.MyArmor;
import org.gera.mymod.block.BlockMyBlock;
import org.gera.mymod.block.BlockMyFurnace;
import org.gera.mymod.block.BlockMyOre;
import org.gera.mymod.generation.MyGenerator;
import org.gera.mymod.item.ItemMyFood;
import org.gera.mymod.item.ItemMyItem;
import org.gera.mymod.item.tool.ItemMyAxeTool;
import org.gera.mymod.item.tool.ItemMyHoeTool;
import org.gera.mymod.item.tool.ItemMyPickaxeTool;
import org.gera.mymod.item.tool.ItemMyShovelTool;
import org.gera.mymod.item.tool.ItemMySwordTool;

import cpw.mods.fml.common.registry.GameRegistry;

public class MyRegistry {
	//Unique IDs
	public static int myHelmetID;
	public static int myChestplateID;
	public static int myLegginsID;
	public static int myBootsID;
	
	//Creative Tabs
	public static CreativeTabs MyTab = new TabMyTab(CreativeTabs.getNextID(), "TabMyTab");
	
	//Tool Materials
	public static ToolMaterial MyMaterial = EnumHelper.addToolMaterial("My Material", 3, 500, 15.0f, 4.0f, 10);
	
	//Armor Materials
	public static ArmorMaterial MyArmorMaterial = EnumHelper.addArmorMaterial("MyArmorMaterial", 40, new int[]	{3, 5, 4 ,2}, 10);
	
	//Blocks
    public static Block MyBlock = new BlockMyBlock(Material.rock).setBlockName("BlockMyBlock").setCreativeTab(MyTab);
    public static Block MyOre = new BlockMyOre(Material.rock).setBlockName("BlockMyOre").setCreativeTab(MyTab);
    
    //Machines
    public static Block MyFurnace_Idle = new BlockMyFurnace(false, Material.iron).setBlockName("BlockMyFurnace_Idle").setCreativeTab(MyTab);
    public static Block MyFurnace_Active = new BlockMyFurnace(true, Material.iron).setBlockName("BlockMyFurnace_Active").setCreativeTab(MyTab);
    
    //Items
    public static Item MyItem = new ItemMyItem().setUnlocalizedName("ItemMyItem").setCreativeTab(MyTab);
    public static Item MyFood = new ItemMyFood(5, 2, true).setUnlocalizedName("ItemMyFood").setCreativeTab(MyTab);
    
    //Tools
    public static Item MyAxe = new ItemMyAxeTool(MyMaterial).setUnlocalizedName("ItemMyAxeTool").setCreativeTab(MyTab);;
    public static Item MyHoe = new ItemMyHoeTool(MyMaterial).setUnlocalizedName("ItemMyHoeTool").setCreativeTab(MyTab);;
    public static Item MyShovel = new ItemMyShovelTool(MyMaterial).setUnlocalizedName("ItemMyShovelTool").setCreativeTab(MyTab);;
    public static Item MyPickaxe = new ItemMyPickaxeTool(MyMaterial).setUnlocalizedName("ItemMyPickaxeTool").setCreativeTab(MyTab);;
    public static Item MySword = new ItemMySwordTool(MyMaterial).setUnlocalizedName("ItemMySwordTool").setCreativeTab(MyTab);;
    
    //Armor
    public static Item myArmorHelmet = new MyArmor(MyArmorMaterial, myHelmetID, 0).setUnlocalizedName("ItemMyArmorHelmet");
    public static Item myArmorChestplate = new MyArmor(MyArmorMaterial, myChestplateID, 1).setUnlocalizedName("ItemMyArmorChestplate");
    public static Item myArmorLeggins = new MyArmor(MyArmorMaterial, myLegginsID, 2).setUnlocalizedName("ItemMyArmorLeggins");
    public static Item myArmorBoots = new MyArmor(MyArmorMaterial, myBootsID, 3).setUnlocalizedName("ItemMyArmorBoots");
    
	// Generators 
	public static MyGenerator MyOreGenerator = new MyGenerator(MyOre, 25, 3, 100);
	
	public static void registerGenerators()	{
		//Generator Registry
		GameRegistry.registerWorldGenerator(MyOreGenerator, 1);
	}
	
	public static void registerStuff()	{

		//Block Registry
		GameRegistry.registerBlock(MyBlock, "BlockMyBlock");
		GameRegistry.registerBlock(MyOre, "BlockMyOre");
		GameRegistry.registerBlock(MyFurnace_Idle, "BlockMyFurnace_Idle");
		GameRegistry.registerBlock(MyFurnace_Active, "BlockMyFurnace_Active");
		
		//Item registry
		GameRegistry.registerItem(MyItem, "ItemMyItem");
		GameRegistry.registerItem(MyFood, "ItemMyFood");
		
		//Tool registry
		GameRegistry.registerItem(MyAxe, "ItemMyAxeTool");
		GameRegistry.registerItem(MyHoe, "ItemMyHoeTool");
		GameRegistry.registerItem(MyShovel, "ItemMyShovelTool");
		GameRegistry.registerItem(MyPickaxe, "ItemMyPickaxeTool");
		GameRegistry.registerItem(MySword, "ItemMySwordTool");
		
		//Armor registry
		GameRegistry.registerItem(myArmorHelmet, "ItemMyArmorHelmet");
		GameRegistry.registerItem(myArmorChestplate, "ItemMyArmorChestplate");
		GameRegistry.registerItem(myArmorLeggins, "ItemMyArmorLeggins");
		GameRegistry.registerItem(myArmorBoots, "ItemMyArmorBoots");
	}
	
	public static void registerCraft()	{
		//Craft recipes registry
		GameRegistry.addRecipe(new ItemStack(MyBlock, 1), new Object []
				{"XXX", "XXX", "XXX", 'X', MyItem});
		GameRegistry.addShapelessRecipe(new ItemStack(MyItem, 9), MyBlock);
		GameRegistry.addRecipe(new ItemStack(MyItem, 3), new Object []
				{"XYX", "YZY", "XYX",
			'X', new ItemStack(Items.dye, 1, 4), 
			'Y', Item.itemRegistry.getObject("redstone"),
			'Z',Item.itemRegistry.getObject("diamond")});
		GameRegistry.addRecipe(new ItemStack(MyItem, 3), new Object []
				{"YXY", "XZX", "YXY",
			'X', new ItemStack(Items.dye, 1, 4), 
			'Y', Item.itemRegistry.getObject("redstone"),
			'Z',Item.itemRegistry.getObject("diamond")});
		
		GameRegistry.addRecipe(new ItemStack(MyAxe, 1), new Object []
				{"XX ", "XY ", " Y ", 'X', MyItem, 'Y', 
			Item.itemRegistry.getObject("stick")});
		GameRegistry.addRecipe(new ItemStack(MySword, 1), new Object []
				{" X ", " X ", " Y ", 'X', MyItem, 'Y', 
			Item.itemRegistry.getObject("stick")});
		GameRegistry.addRecipe(new ItemStack(MyShovel, 1), new Object []
				{" X ", " Y ", " Y ", 'X', MyItem, 'Y',
			Item.itemRegistry.getObject("stick")});
		GameRegistry.addRecipe(new ItemStack(MyPickaxe, 1), new Object []
				{"XXX", " Y ", " Y ", 'X', MyItem, 'Y',
			Item.itemRegistry.getObject("stick")});
		GameRegistry.addRecipe(new ItemStack(MyHoe, 1), new Object []
				{"XX ", " Y ", " Y ", 'X', MyItem, 'Y',
			Item.itemRegistry.getObject("stick")});
		
		GameRegistry.addRecipe(new ItemStack(myArmorHelmet, 1), new Object []
				{"XXX", "X X", 'X', MyItem});
		GameRegistry.addRecipe(new ItemStack(myArmorChestplate, 1), new Object []
				{"X X", "XXX", "XXX", 'X', MyItem});
		GameRegistry.addRecipe(new ItemStack(myArmorLeggins, 1), new Object []
				{"XXX", "X X", "X X", 'X', MyItem});
		GameRegistry.addRecipe(new ItemStack(myArmorBoots, 1), new Object []
				{"X X", "X X", 'X', MyItem});
		
		//Smelting recipes registry
		GameRegistry.addSmelting(MyItem, new ItemStack(MyFood, 1), 5);
	}
}
